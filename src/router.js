import React from 'react';
import { createBrowserRouter } from 'react-router-dom';
import App from './App';
import Cart from './component/Cart';
import HomePage from './component/HomePage';
import AddProduct from './component/AddProduct';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: '/marketPlace',
        element: <HomePage />,
        // children: [
        //   {
        //     path: '/dashboard/profiles/:id',
        //     element: <ProfilePage />,
        //   },
        // ],
      },
      {
        path: '/cart',
        element: <Cart />,
      },
      {
        path: '/addProduct',
        element: <AddProduct />,
      },
    ],
  },
]);

export default router;

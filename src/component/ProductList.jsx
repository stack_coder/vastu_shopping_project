import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { deleteProduct, removeProduct } from '../store/productSlice'; // Adjust the path as necessary
import './productlist.css';

const ProductList = ({ handleEdit }) => {
  const products = useSelector((state) => state.product.products);
  const dispatch = useDispatch();

  const handleDelete = (id) => {
    dispatch(removeProduct(id));
  };

  return (
    <div className='product-list'>
      {products.length === 0 ? (
        <p>No products available</p>
      ) : (
        <ul>
          {products.map((product) => (
            <li key={product.id} className='product-item'>
              <div className='product-details'>
                <h2>{product.name}</h2>
                <p>Category: {product.category}</p>
                <p>Price: ${product.price}</p>
                <p>Quantity: {product.quantity}</p>
              </div>
              <button
                className='btn btn-danger'
                onClick={() => handleEdit(product)}
              >
                Edit
              </button>
              <button
                className='btn btn-danger'
                onClick={() => handleDelete(product.id)}
              >
                Delete
              </button>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default ProductList;

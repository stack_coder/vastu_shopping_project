import React from 'react';
import useCart from '../hooks/useCart';
import ProductCard from './ProductCard';
// import productList from '../dummyData/productList';
import { useNavigate } from 'react-router-dom';
import useProduct from '../hooks/useProduct';

const Cart = () => {
  const { removeCart, cartItemCount, cartItem } = useCart();
  const { products } = useProduct();

  const navigate = useNavigate();

  const ids = cartItem.map((product) => product.id);
  const countLookup = cartItemCount.reduce((acc, item) => {
    acc[item.id] = item.count;
    return acc;
  }, {});
  const getCartDetail = products
    ?.map((item) => {
      if (ids.includes(item.id)) {
        return { ...item, count: countLookup[item.id] || 0 };
      }
    })
    .filter((ite) => ite);
  const totalAmount = getCartDetail?.reduce((acc, curr) => {
    acc += curr.price * curr.count;
    return acc;
  }, 0);
  const formattedTotalAmount = totalAmount.toFixed(2);
  return (
    <>
      <button onClick={removeCart}>Clean Cart</button>
      <div class='product-list-container'>
        <div class='product-list'>
          {getCartDetail.map(
            (product, index) =>
              product.count > 0 && (
                <ProductCard key={index} product={product} text='cart' />
              )
          )}
        </div>
      </div>
      <h3>{formattedTotalAmount}</h3>
      <button onClick={() => navigate('/')}>back</button>
    </>
  );
};

export default Cart;

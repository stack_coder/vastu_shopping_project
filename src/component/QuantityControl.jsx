import React from 'react';
import { FaPlus, FaMinus } from 'react-icons/fa';

const QuantityControl = ({
  count,
  setCount,
  updateCount,
  productId,
  product,
}) => (
  <div className='quantity-control'>
    <button
      className='btn btn-primary'
      disabled={count === product.quantity}
      onClick={() => {
        setCount((prevCount) => {
          const newCount = prevCount + 1;
          updateCount(productId, newCount);
          return newCount;
        });
      }}
    >
      <FaPlus />
    </button>
    <span>{count}</span>
    <button
      className='btn btn-primary'
      disabled={count === 1}
      onClick={() => {
        setCount((prevCount) => {
          const newCount = prevCount - 1;
          updateCount(productId, newCount);
          return newCount;
        });
      }}
    >
      <FaMinus />
    </button>
  </div>
);

export default QuantityControl;

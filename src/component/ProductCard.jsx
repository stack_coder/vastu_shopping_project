import React, { useState } from 'react';
import './product.css';
import useCart from '../hooks/useCart';
import QuantityControl from './QuantityControl';
import { useNavigate } from 'react-router-dom';

// Reusable component for quantity control

// Main ProductCard component
const ProductCard = ({ product, text, SetOpen }) => {
  const { AddItemToCart, removeItemFromCart, updateProductCount } = useCart();
  const [count, setCount] = useState(product.count || 1); // default to 1 if count is not defined
  const navigate = useNavigate();

  const handleAddToCart = () => {
    AddItemToCart(product.id, count);
  };

  const handleRemoveFromCart = () => {
    setCount(1);
    removeItemFromCart(product.id);
  };

  return (
    <div className='card'>
      <img src={product.image} alt={product.name} className='card-img-top' />
      <div className='card-body'>
        <h5 className='card-title'>{product.name}</h5>
        <p className='card-text'>{product.description}</p>
        <p className='card-price'>${product.price}</p>

        {text === 'cart' && (
          <p className='card-price'>Quantity: {product.count}</p>
        )}

        {text === 'cart' && (
          <QuantityControl
            count={count}
            setCount={setCount}
            updateCount={updateProductCount}
            productId={product.id}
            product={product}
          />
        )}

        {text === 'home' && (
          <div className='action-buttons'>
            <button
              className='btn btn-primary'
              onClick={handleAddToCart}
              disabled={product.count > 0}
            >
              {product.count > 0 ? 'Added to cart' : 'Add to Cart'}
            </button>
            {product.count > 0 && (
              <>
                <button
                  className='btn btn-primary'
                  onClick={() => navigate('/cart')}
                >
                  Go to Cart
                </button>
                <button
                  className='btn btn-primary'
                  onClick={handleRemoveFromCart}
                >
                  Remove from Cart
                </button>
              </>
            )}
          </div>
        )}

        {text === 'cart' && (
          <button className='btn btn-primary' onClick={handleRemoveFromCart}>
            Remove from Cart
          </button>
        )}
      </div>
    </div>
  );
};

export default ProductCard;

import React from 'react';
import './addproduct.css';
import ProductList from './ProductList';
import useProduct from '../hooks/useProduct';

const AddProduct = () => {
  const {
    setView,
    formData,
    setFormData,
    setEditMode,
    view,
    handleSubmit,
    handleChange,
    editMode,
  } = useProduct();

  // Handle edit button click
  const handleEdit = (product) => {
    setView('add');
    setFormData({
      id: product.id,
      name: product.name,
      category: product.category,
      price: product.price,
      quantity: product.quantity,
    });
    setEditMode(true);
  };

  return (
    <>
      <header className='header'>
        <button
          onClick={() => {
            setView('add');
            setEditMode(false);
          }}
          className='btn btn-primary'
        >
          Add Product
        </button>
        <button onClick={() => setView('list')} className='btn btn-secondary'>
          Product List
        </button>
      </header>
      {view === 'list' && <ProductList handleEdit={handleEdit} />}
      {view === 'add' && (
        <form className='product-form' onSubmit={handleSubmit}>
          <div className='form-group'>
            <label htmlFor='name'>Product Name:</label>
            <input
              type='text'
              id='name'
              name='name'
              value={formData.name}
              onChange={handleChange}
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='category'>Category:</label>
            <input
              type='text'
              id='category'
              name='category'
              value={formData.category}
              onChange={handleChange}
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='price'>Price:</label>
            <input
              type='number'
              id='price'
              name='price'
              value={formData.price}
              onChange={handleChange}
              step='0.01'
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='quantity'>Quantity:</label>
            <input
              type='number'
              id='quantity'
              name='quantity'
              value={formData.quantity}
              onChange={handleChange}
              required
            />
          </div>

          <button type='submit' className='btn btn-primary'>
            {editMode ? 'Update' : 'Submit'}
          </button>
        </form>
      )}
    </>
  );
};

export default AddProduct;

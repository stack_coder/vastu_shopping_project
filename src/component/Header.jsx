import React from 'react';
import './header.css';
import useCart from '../hooks/useCart';
import { useNavigate } from 'react-router-dom';
const Header = () => {
  const { cartItem } = useCart();
  const navigate = useNavigate();

  return (
    <header class='header'>
      <div class='logo' onClick={() => navigate('/')}>
        Home
      </div>
      <div class='cart' onClick={() => navigate('/cart')}>
        <div class='logo'>Cart</div>
        {cartItem.length !== 0 && (
          <>
            <i class='fas fa-shopping-cart'></i>
            <span class='item-count'>{cartItem.length}</span>
          </>
        )}
      </div>
      <div class='cart' onClick={() => navigate('/addproduct')}>
        <div class='logo'>Product Admin</div>
      </div>
    </header>
  );
};

export default Header;

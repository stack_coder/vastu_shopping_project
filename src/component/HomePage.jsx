import React from 'react';
// import productList from '../dummyData/productList';
import ProductCard from './ProductCard';
import './product.css';
import useCart from '../hooks/useCart';
import useProduct from '../hooks/useProduct';
const HomePage = ({ SetOpen }) => {
  const { cartItemCount } = useCart();
  const { products } = useProduct();
  const countLookup = cartItemCount.reduce((acc, item) => {
    acc[item.id] = item.count;
    return acc;
  }, {});
  const productCount = products?.map((prod) => ({
    ...prod,
    count: countLookup[prod.id] || 0,
  }));

  return (
    <div class='product-list-container'>
      <div class='product-list'>
        {productCount.map((product, index) => (
          <ProductCard
            key={index}
            product={product}
            text='home'
            SetOpen={SetOpen}
          />
        ))}
      </div>
    </div>
  );
};

export default HomePage;

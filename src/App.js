import HomePage from './component/HomePage';
import Header from './component/Header';
import { Outlet, useLocation } from 'react-router-dom';

function App() {
  const { pathname } = useLocation();
  return (
    <>
      <Header />
      {pathname === '/' && <HomePage />}
      <Outlet />
    </>
  );
}

export default App;

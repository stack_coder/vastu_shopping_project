import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  products: [],
};

const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.products.push(action.payload);
    },
    updateProduct: (state, action) => {
      const product = action.payload;
      state.products = state.products.map((item) =>
        item.id === product.id ? { ...item, ...product } : item
      );
    },
    removeProducts: (state) => {
      state.products = [];
    },
    removeProduct: (state, action) => {
      const id = action.payload;
      state.products = state.products.filter((item) => item.id !== id);
    },
  },
});

export const { addProduct, removeProducts, removeProduct, updateProduct } =
  productSlice.actions;

export default productSlice.reducer;

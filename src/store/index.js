import { configureStore } from '@reduxjs/toolkit';
import cartReducer from './cartSlice/index.js';
import productReducer from './productSlice/index.js';

const store = configureStore({
  reducer: {
    cart: cartReducer,
    product: productReducer,
  },
});

export default store;

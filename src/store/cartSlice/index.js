import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  items: [],
  itemCounts: [],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addCart: (state, action) => {
      const id = action.payload;
      state.items.push({ id });
    },
    removeFromCart: (state) => {
      state.items = [];
      state.itemCounts = [];
    },
    removeItem: (state, action) => {
      const id = action.payload;
      state.items = state.items.filter((item) => item.id !== id);
      state.itemCounts = state.itemCounts.filter((item) => item.id !== id);
    },
    ItemCount: (state, action) => {
      const countData = action.payload;
      const { id, text } = countData;
      // Update the count for the specific item
      state.items = state.items.map((item) => {
        if (item.id === id) {
          const newCount = text === 'inc' ? item.count + 1 : item.count - 1;
          return { ...item, count: Math.max(0, newCount) }; // Ensure count doesn't go negative
        }
        return item;
      });

      // Log the updated items for debugging
      state.itemCounts.push({ id: countData.id, count: countData.count });
    },
  },
});

export const { addCart, removeFromCart, removeItem, ItemCount } =
  cartSlice.actions;

export default cartSlice.reducer;

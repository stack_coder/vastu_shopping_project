import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { addProduct, updateProduct } from '../store/productSlice';
import { v4 as uuidv4 } from 'uuid'; // Import the UUID function

const useProduct = () => {
  const [formData, setFormData] = useState({
    id: '',
    name: '',
    category: '',
    price: 0,
    quantity: 0,
  });
  const [editMode, setEditMode] = useState(false);

  const [view, setView] = useState('list');

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const products = useSelector((state) => state.product.products);

  // Handle input changes
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  // Handle form submission
  const handleSubmit = (e) => {
    e.preventDefault();
    const productWithId = {
      ...formData,
      price: parseFloat(formData.price), // Convert price to number
      quantity: parseInt(formData.quantity, 10), // Convert quantity to integer
    };

    if (editMode) {
      dispatch(updateProduct(productWithId)); // Dispatch update product action
    } else {
      dispatch(addProduct({ ...productWithId, id: uuidv4() })); // Add new product with unique ID
    }

    // Reset the form and state
    setFormData({
      id: '',
      name: '',
      category: '',
      price: 0,
      quantity: 0,
    });
    setEditMode(false);
    setView('list');
    navigate('/');
  };
  return {
    products,
    handleSubmit,
    handleChange,
    view,
    setView,
    editMode,
    setEditMode,
    formData,
    setFormData,
  };
};

export default useProduct;

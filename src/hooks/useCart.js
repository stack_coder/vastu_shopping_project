import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  ItemCount,
  addCart,
  removeFromCart,
  removeItem,
} from '../store/cartSlice';

const useCart = () => {
  const cartItem = useSelector((state) => state.cart.items);
  const cartItemCount = useSelector((state) => state.cart.itemCounts);

  const dispatch = useDispatch();

  const AddItemToCart = (id, count) => {
    dispatch(addCart(id));
    dispatch(ItemCount({ id, count }));
  };

  const updateProductCount = (id, count) => {
    dispatch(ItemCount({ id, count }));
  };
  const removeCart = () => {
    dispatch(removeFromCart());
  };

  const removeItemFromCart = (id) => {
    dispatch(removeItem(id));
  };
  return {
    cartItem,
    cartItemCount,
    AddItemToCart,
    removeCart,
    removeItemFromCart,
    updateProductCount,
  };
};

export default useCart;

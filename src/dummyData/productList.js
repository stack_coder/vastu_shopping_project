const productList = [
  {
    id: 1,
    name: 'Laptop',
    category: 'Electronics',
    price: 999.99,
    quantity: 10,
  },
  {
    id: 2,
    name: 'Smartphone',
    category: 'Electronics',
    price: 699.99,
    quantity: 8,
  },
  {
    id: 3,
    name: 'Desk Chair',
    category: 'Furniture',
    price: 89.99,
    quantity: 15,
  },
  { id: 4, name: 'Book', category: 'Books', price: 19.99, quantity: 9 },
  { id: 5, name: 'T-shirt', category: 'Clothing', price: 14.99, quantity: 12 },
];

export default productList;
